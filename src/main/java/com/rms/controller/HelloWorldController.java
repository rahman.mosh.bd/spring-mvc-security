package com.rms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {
    @GetMapping("/hello-world")
    public String helloWorld() {
        return "hello-world" ;
    }

    @GetMapping("/signup")
    @ResponseBody
    public String signup() {
        return "registration done" ;
    }

    @GetMapping("/signout")
    @ResponseBody
    public String signout() {
        return "logout successfully" ;
    }
}
