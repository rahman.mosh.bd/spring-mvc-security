package com.rms.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/greet")
public class MyRestController {
    @GetMapping("{name}")
    public String greet(@PathVariable("name") String name) {
        return "Good Morning "+name ;
    }
}
