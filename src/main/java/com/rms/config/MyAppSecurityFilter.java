package com.rms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity(debug = true)
public class MyAppSecurityFilter extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder bcryptPasswordEncoder ;
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("mosh")
                .password(bcryptPasswordEncoder.encode("mosh"))
                .roles("admin");
    }

    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/signup").authenticated()
                .antMatchers("/signout").authenticated()
                .antMatchers("/hello-world").permitAll()
                .and()
                .formLogin()
                .and()
                .httpBasic();
    }
}
